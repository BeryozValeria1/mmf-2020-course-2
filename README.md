# Список заданий
--------------------
* ### Оптимизированный бэкэнд под сайт с отелями [Optimized](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/optimized/)
* ### Неоптимизированный бэкэнд под сайт с отелями + тест [Unoptimized](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/unoptimized/)
* ### Результаты скорости выполнения оптимизации [Result](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/Result.xlsx)
* ### Сайт записей на отель [WebPage with Flask](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/webPage/)
* ### Визуализация результатов [Visualization](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/Visualization/)
* ### Инд. зад. №1 [вариант 1](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/lab_1/)
* ### Инд. зад. №2 [вариант 1](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/lab_2/)
* ### Инд. зад. №3 [вариант 7](https://bitbucket.org/BeryozValeria1/mmf-2020-course-2/src/master/lab_3/)




