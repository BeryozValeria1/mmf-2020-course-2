def findNumber(arr):
    temp = set()
    ans = set()
    
    for i in range(0, len(arr)):
        if arr[i] in temp:
            ans.add(arr[i])
        else:
            temp.add(arr[i])
    
    for i in range(0, len(arr)):
        if (arr[i] in temp and arr[i] in ans):
            return ans
        else:
            pass
    return None
        