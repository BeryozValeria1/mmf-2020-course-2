import Test
from solve import Un

Test.equal(Un([1, 9, 8, 8, 7, 6, 1, 6]),                [9, 7])
Test.equal(Un([5, 5, 2, 4, 4, 4, 9, 9, 9, 1]),          [2, 1])
Test.equal(Un([9, 5, 6, 8, 7, 7, 1, 1, 1, 1, 1, 9, 8]), [5, 6])
Test.equal(Un([9, 6, 3, 2, 6, 4, 9, 6, 3, 2, 0]),       [4, 0])
Test.equal(Un([1, 2, 3, 4, 6, 7, 1, 2, 3, 4]),          [6, 7])
Test.equal(Un([5, 6, 6, 5, 6, 9, 9, 1, 0]),             [5, 0])
Test.equal(Un([2, 3, 5, 1, 2, 3, 4, 4, 5, 6]),          [5, 6])
Test.equal(Un([11, 11, 12, 13, 14, 13]),                [14, 13])
Test.equal(Un([2, 3]),                                  [3, 2])
Test.equal(Un([3, 4, 5, 5, 4, 6]),                      [4, 6])