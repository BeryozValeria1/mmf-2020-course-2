from datetime import datetime

dataFormat = '%Y-%m-%d %H:%M:%S'


def findById(data, ResourseID, StartDateTime, EndDateTime):
    flag = True
    
    for i in range(1, len(data)):
        if data[i][0] == ResourseID:
            if (
                data[i][1] <= StartDateTime < data[i][2]
                or data[i][1] < EndDateTime <= data[i][2]
                or (StartDateTime <= data[i][1] and data[i][2] <= EndDateTime)
            ):
                flag = False
                break
    
    return flag



def searchID(data, StartDateTime, EndDateTime):
    ID = {}

    for i in range(1, len(data)):
        if not (
                data[i][1] <= StartDateTime < data[i][2]
                or data[i][1] < EndDateTime <= data[i][2]
                or (StartDateTime <= data[i][1] and data[i][2] <= EndDateTime)
            ):
            if data[i][0] not in ID:
                ID.update({data[i][0]: 1})
        else: ID.update({data[i][0]: -1})

    return ID

file = "input.txt"
        
def readingData(file):
    dataFormat = '%Y-%m-%d %H:%M:%S'

    f = open(file, "r")

    data = []

    for line in f.readlines():
        data.append(line)

    data[0] = data[0][:-1]
    data[0] = data[0].split(',')

    for i in range(1, len(data)):
        data[i] = data[i].split(',')
        data[i][1] = data[i][1][:data[i][1].rfind('.')]
        data[i][1] = datetime.strptime(data[i][1], dataFormat)

        data[i][2] = data[i][2][:data[i][2].rfind('.')]
        data[i][2] = datetime.strptime(data[i][2], dataFormat)
    # print(data[i])
    
    f.close()

    return data 



def main():

    ResourseID = input("ID: ")
    print(f'Введите начало и конец даты в формате "{dataFormat}"')
    StartDateTime = str(input("Начало: "))
    EndDateTime = str(input("Конец: "))

    StartDateTime = datetime.strptime(StartDateTime, dataFormat)
    EndDateTime = datetime.strptime(EndDateTime, dataFormat)

    data = readingData(file)

    if findById(data, ResourseID, StartDateTime, EndDateTime):
        print("ID свободен")
    else:
        print("ID занят")

    print("Поиск свободных ID по заданному времени: ")
    ans = searchID(data, StartDateTime, EndDateTime)

    for key in ans:
        if (not (ans[key] == -1)):
            print(key)


if __name__ == "__main__":
    main()