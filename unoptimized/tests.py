import solve

# print(data)

def test1():
    """---- test function findById ----"""
    print('---- test function findById ----')
    print('start test1 - valid')
    data = solve.readingData("input1.txt")
    answerFromFile = solve.readingData("output1.txt")
    ans = solve.findById(data, answerFromFile[1][0], answerFromFile[1][1], answerFromFile[1][2])
    if ans:
        print("valid") 
    else: 
        print("invlid")
    
def test2():
    print('start test2 - invalid')
    data = solve.readingData("input2.txt")
    answerFromFile = solve.readingData("output2.txt")
    ans = solve.findById(data, answerFromFile[1][0], answerFromFile[1][1], answerFromFile[1][2])
    # print( answerFromFile[1])
    if ans:
        print("valid") 
    else: 
        print("invalid")

def test3():
    """---- test function searchID ----"""
    print('---- test function searchID ----')
    print('start test3 - valid')
    data = solve.readingData("input3.txt")
    answerFromFile = solve.readingData("output3.txt")
    ans = solve.searchID(data, answerFromFile[1][1], answerFromFile[1][2])
    
    resID = []
    for key in ans:
        if ans[key] == 1: 
            resID.append(key)

    print('After searchig: ')
    print( answerFromFile[0])
    print('From file: ')
    print( resID) 
    

    if len(list(set(resID) - set(answerFromFile[0]))) == 0 and len(list(set(answerFromFile[0]) - set(resID))) == 0:
        print("valid")
    else: 
        print("invalid")

def test4():
    """---- test function searchID ----"""
    print('---- test function searchID ----')
    print('start test4 - invalid')
    data = solve.readingData("input4.txt")
    answerFromFile = solve.readingData("output4.txt")
    ans = solve.searchID(data, answerFromFile[1][1], answerFromFile[1][2])
    
    resID = []
    for key in ans:
        if ans[key] == 1: 
            resID.append(key)

    print('After searchig: ')
    print( answerFromFile[0])
    print('From file: ')
    print( resID) 
    

    if len(list(set(resID) - set(answerFromFile[0]))) == 0 and len(list(set(answerFromFile[0]) - set(resID))) == 0:
        print("valid")
    else: 
        print("invalid")

test1()
test2()
test3()
test4()

